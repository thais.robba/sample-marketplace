export const emptyCallback = () => {};

export const formatAsCurrency = value => {
  const formattedValue = (value / 100)
    .toFixed(2)
    .split(".")
    .map(part => part.split(/(?=(?:...)*$)/).join("."))
    .join(",");

  return `R$ ${formattedValue}`;
};
