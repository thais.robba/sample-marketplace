import Cart from "./pages/Cart";
import Checkout from "./pages/Checkout";
import Home from "./pages/Home";
import ProductDetails from "./pages/ProductDetails";

export const paths = {
  home: "/",
  cart: "/cart",
  checkout: "/checkout",
  productDetails: "/products/:id"
};

const routes = [
  {
    path: paths.cart,
    component: Cart
  },
  {
    path: paths.checkout,
    component: Checkout
  },
  {
    path: paths.productDetails,
    component: ProductDetails
  },
  {
    path: paths.home,
    component: Home,
    exact: true
  }
];

export default routes;
