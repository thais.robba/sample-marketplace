/**
 * 2. Detalhe do produto
      - Nome do produto
      - Preço do produto
      - Descrição do produto
      - Id do vendedor (`recipient_id` que será passado na _splitrule_)
      - Uma imagem do produto
      - Botão para adicionar no carrinho
 */

import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import AppBar from "../../components/AppBar";
import FooterBar from "../../components/FooterBar";
import BackButton from "../../components/BackButton";

import { test } from "../../constants";
import { paths } from "../../routes";
import { Paper, Typography, Grid } from "@material-ui/core";
import { formatAsCurrency } from "../../utils";
import AddProductButton from "../../components/AddProductButton";

const ProductDetails = () => {
  let { id } = useParams();
  const [product, setProduct] = useState();

  useEffect(() => {
    // We would probably fetch extra information from the server
    // but since all data is mocked, we keep it simple
    const result = test.products.find(entry => entry.id === id);
    result.description =
      "Medidas: 15cm x 15cm.\nImpresso em papel especial, extraído de maneira sustentável por esquilos treinados.\nNão comestível.";
    setProduct(result);
  }, [id]);

  return (
    <>
      <AppBar>
        <BackButton to={paths.home}></BackButton>
      </AppBar>

      {!product ? (
        <p>Buscando informações do sticker...</p>
      ) : (
        <>
          <Paper style={{ padding: "72px 4vw" }}>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid item xs={12}>
                <Typography
                  style={{ fontFamily: "Calistoga" }}
                  align="center"
                  variant="h3"
                >
                  {product.name}
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <img
                  src={product.src}
                  style={{ width: "100%", maxWidth: "256px", margin: "16px 0" }}
                />
              </Grid>
              <Grid item xs={12}>
                <Typography variant="h6" align="center">
                  era <s>{formatAsCurrency(product.value)}</s>
                </Typography>
                <Typography variant="h4" align="center">
                  {formatAsCurrency(product.price)}
                </Typography>
              </Grid>
              <Grid item xs={12} style={{ margin: "16px 0 0 0" }}>
                <Typography
                  align="center"
                  style={{ whiteSpace: "pre-line", maxWidth: "560px" }}
                >
                  {product.description}
                </Typography>
              </Grid>
              <Grid item xs={12} style={{ margin: "24px 0 0 0" }}>
                <Typography align="center">
                  Vendido por {product.recipient_id}
                </Typography>
              </Grid>
            </Grid>
          </Paper>
          <FooterBar>
            <AddProductButton product={product}></AddProductButton>
          </FooterBar>
        </>
      )}
    </>
  );
};

export default ProductDetails;
