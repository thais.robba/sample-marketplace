import store from "./store";
import { test } from "../../constants";

describe("Home", () => {
  it("should properly fetch products", () => {
    expect(store.products).toEqual([]);

    //If this was an actual async call, we would want to mock the return value
    store.fetchProducts();

    expect(store.products).toEqual(test.products);
  });
});
