import React, { useEffect } from "react";
import { view } from "react-easy-state";

import store from "./store";
import HomeView from "./HomeView";

const Home = view(() => {
  useEffect(() => {
    store.fetchProducts();
  }, []);

  return <HomeView products={store.products}></HomeView>;
});

export default Home;
