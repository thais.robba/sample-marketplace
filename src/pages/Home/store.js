import { store } from "react-easy-state";
import { test } from "../../constants";

const homeStore = store({
  products: [],
  async fetchProducts() {
    //Normally in here we would make an API request but
    //since we are mocking the data, here we go!
    homeStore.products = test.products;
  }
});

export default homeStore;
