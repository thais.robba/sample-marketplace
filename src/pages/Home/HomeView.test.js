import React from "react";
import { render } from "@testing-library/react";

import HomeView from "./HomeView";

import { MemoryRouter } from "react-router";

import { test } from "./constants";
import { test as globals } from "../../constants";

describe("HomeView", () => {
  it("should properly show a minimum of 10 entries", () => {
    const { asFragment, getAllByTestId } = render(
      <MemoryRouter>
        <HomeView products={globals.products}></HomeView>
      </MemoryRouter>
    );

    const products = getAllByTestId(test.ids.item);
    expect(products.length).toBeGreaterThanOrEqual(test.entries.minimum);

    expect(asFragment()).toMatchSnapshot();
  });

  it("should degrade gracefully with no entries", () => {
    const { asFragment, getByTestId } = render(
      <MemoryRouter>
        <HomeView></HomeView>
      </MemoryRouter>
    );

    getByTestId(test.ids.noResults);

    expect(asFragment()).toMatchSnapshot();
  });
});
