import React from "react";
import { Grid, Typography, useMediaQuery } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";

import AppBar from "../../components/AppBar";
import ProductCard from "../../components/ProductCard";
import ViewCartButton from "../../components/ViewCartButton";
import { test, locale } from "./constants";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(10, 2)
  }
}));

const NoResults = () => {
  return (
    <Typography variant="h2" align="center" data-testid={test.ids.noResults}>
      {locale.pt.noResults}
    </Typography>
  );
};

const ProductList = ({ products }) => {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.only("xs"));

  return (
    <ul data-testid={test.ids.item}>
      <Grid
        container
        spacing={2}
        alignItems="center"
        direction={matches ? "column" : "row"}
        justify="space-around"
      >
        {products.map((product, i) => (
          <Grid item xs={12} sm={6} md={4} lg={3} xg={2} key={i}>
            <ProductCard product={product}></ProductCard>
          </Grid>
        ))}
      </Grid>
    </ul>
  );
};

const HomeView = ({ products }) => {
  const classes = useStyles();
  const hasProducts = products && products.length > 1;

  return (
    <div className={classes.root}>
      <AppBar>
        <ViewCartButton />
      </AppBar>

      {hasProducts ? (
        <ProductList products={products}></ProductList>
      ) : (
        <NoResults></NoResults>
      )}
    </div>
  );
};

export default HomeView;
