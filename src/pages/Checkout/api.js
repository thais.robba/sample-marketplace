import pagarme from "pagarme";

// Welp, we are using pretty much hardcoded values here - next step would definetly be
// actually using the checkout form values
export const postTransaction = (amount, products) => {
  const items = products.map(({ id, price, quantity, name }) => {
    return {
      id,
      unit_price: price,
      quantity,
      title: name,
      tangible: true
    };
  });

  return pagarme.client.connect({ api_key: process.env.API_KEY }).then(client =>
    client.transactions.create({
      amount,
      card_number: "4111111111111111",
      card_cvv: "123",
      card_expiration_date: "0922",
      card_holder_name: "Morpheus Fishburne",
      customer: {
        external_id: "#3311",
        name: "Morpheus Fishburne",
        type: "individual",
        country: "br",
        email: "mopheus@nabucodonozor.com",
        documents: [
          {
            type: "cpf",
            number: "30621143049"
          }
        ],
        phone_numbers: ["+5511999998888", "+5511888889999"],
        birthday: "1965-01-01"
      },
      billing: {
        name: "Trinity Moss",
        address: {
          country: "br",
          state: "sp",
          city: "Cotia",
          neighborhood: "Rio Cotia",
          street: "Rua Matrix",
          street_number: "9999",
          zipcode: "06714360"
        }
      },
      shipping: {
        name: "Neo Reeves",
        fee: 1000,
        delivery_date: "2000-12-21",
        expedited: true,
        address: {
          country: "br",
          state: "sp",
          city: "Cotia",
          neighborhood: "Rio Cotia",
          street: "Rua Matrix",
          street_number: "9999",
          zipcode: "06714360"
        }
      },
      split_rules: [
        {
          percentage: "15",
          recipient_id: "re_ck3dy25em08qb5p6fj75aur1h",
          charge_processing_fee: false
        },
        {
          percentage: "85",
          recipient_id: "re_ck3dy7d3v08wyfa6ey7q9j561",
          charge_processing_fee: true
        }
      ],
      items
    })
  );
};
