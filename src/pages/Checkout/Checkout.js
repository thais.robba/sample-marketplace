import { postTransaction } from "./api";
import React from "react";
import { view } from "react-easy-state";

import CheckoutView from "./CheckoutView";

import globalStore from "../../store";

const Checkout = view(() => {
  const [completed, setCompleted] = useState(false);
  return (
    <CheckoutView
      products={globalStore.cartItems}
      completed={completed}
      onSubmit={(_, { setSubmitting }) => {
        postTransaction(globalStore.totalCartCost, globalStore.cartItems).then(
          _ => {
            // Everything is working fine but ideally, edge cases would be handled here as well.
            // Timeout, invalid purchases, perhaps products have been depleted...
            setSubmitting(false);
            setCompleted(true);
          }
        );
      }}
    ></CheckoutView>
  );
});

export default Checkout;
