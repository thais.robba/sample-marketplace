import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Typography,
  Button,
  Grid,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  CircularProgress
} from "@material-ui/core";
import PaymentIcon from "@material-ui/icons/Payment";
import SentimentVerySatisfiedIcon from "@material-ui/icons/SentimentVerySatisfied";
import { Formik } from "formik";

import BackButton from "../../components/BackButton";
import AppBar from "../../components/AppBar";
import FooterBar from "../../components/FooterBar";
import { paths } from "../../routes";
import { formatAsCurrency } from "../../utils";
import globalStore from "../../store";

const ConfirmPaymentButton = ({ loading, label }) => {
  return (
    <Button
      type="submit"
      endIcon={!loading ? <SentimentVerySatisfiedIcon /> : null}
      fullWidth
      disabled={loading}
    >
      {loading ? <CircularProgress color="secondary" /> : label}
    </Button>
  );
};

const ConfirmationModal = ({ show, products }) => {
  return (
    <Dialog
      open={show}
      onClose={globalStore.emptyCart}
      aria-labelledby="responsive-dialog-title"
    >
      <Typography
        variant="h2"
        align="center"
        justify="center"
        style={{ fontFamily: "Calistoga", padding: "18px 0 18px 0" }}
      >
        Sucesso!
      </Typography>
      <Typography variant="h1" align="center" justify="center">
        🎉
      </Typography>

      <DialogContent>
        <DialogContentText>
          <Typography align="center">• • • </Typography>
          <ul>
            {products.map(({ quantity, name, price }, i) => (
              <li key={i}>
                {`${quantity}x ${name} por ${formatAsCurrency(price)} cada`}
              </li>
            ))}
          </ul>
          <Typography align="center">• • •</Typography>
          <Typography>{`Recebido pelo vendedor: ${formatAsCurrency(
            globalStore.totalCartCost * 0.85
          )}`}</Typography>
          <Typography>{`Taxas: ${formatAsCurrency(
            globalStore.totalCartCost * 0.15
          )}`}</Typography>
          <Typography>
            {`Total: ${formatAsCurrency(globalStore.totalCartCost)}`}
          </Typography>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          component={Link}
          to={paths.home}
          fullWidth
          onClick={globalStore.emptyCart}
          color="primary"
        >
          Yay!
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const Field = ({
  type = "text",
  name,
  label,
  onChange,
  onBlur,
  values,
  errors,
  touched
}) => {
  return (
    <>
      <Typography>{label}</Typography>
      <input
        type={type}
        name={name}
        onChange={onChange}
        onBlur={onBlur}
        value={values[name]}
      />
      <Typography color="secondary">
        {errors[name] && touched[name] && errors[name]}
      </Typography>
    </>
  );
};

const validations = {
  email: value => {
    if (!value) {
      return "Email é obrigatório!";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value)) {
      return "Email inválido!";
    }
  }
};

const initialValues = {
  email: "",
  fullname: "",
  phone: "",
  zipcode: "",
  street: "",
  street_number: "",
  complement: "",
  neighborhood: "",
  city: "",
  state: "",
  card_holder_name: "",
  card_number: "",
  card_cvv: "",
  date: ""
};

const CheckoutForm = ({ onSubmit }) => {
  return (
    <Formik
      initialValues={initialValues}
      validate={values => {
        const errors = {};

        Object.keys(initialValues).forEach(key => {
          if (validations[key]) errors[key] = validations[key](values[key]);
        });

        return errors;
      }}
      onSubmit={onSubmit}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
      }) => (
        <form onSubmit={handleSubmit}>
          <Typography variant="h5">Sobre você :)</Typography>
          <Field
            name="fullname"
            onChange={handleChange}
            onBlur={handleBlur}
            values={values}
            touched={touched}
            errors={errors}
            label="Nome completo"
          />
          <Field
            type="email"
            name="email"
            onChange={handleChange}
            onBlur={handleBlur}
            touched={touched}
            values={values}
            errors={errors}
            label="Email"
          />
          <Field
            name="phone"
            onChange={handleChange}
            onBlur={handleBlur}
            values={values}
            touched={touched}
            errors={errors}
            label="Telefone"
          />
          <Typography variant="h5" style={{ marginTop: "32px" }}>
            Endereço de entrega?
          </Typography>
          <Field
            name="zipcode"
            onChange={handleChange}
            onBlur={handleBlur}
            values={values}
            touched={touched}
            errors={errors}
            label="CEP"
          />
          <Field
            name="street"
            onChange={handleChange}
            onBlur={handleBlur}
            touched={touched}
            values={values}
            errors={errors}
            label="Rua"
          />
          <Field
            name="street_number"
            onChange={handleChange}
            onBlur={handleBlur}
            touched={touched}
            values={values}
            errors={errors}
            label="Número"
          />
          <Field
            name="complement"
            onChange={handleChange}
            onBlur={handleBlur}
            values={values}
            touched={touched}
            errors={errors}
            label="Complemento"
          />
          <Field
            name="neighborhood"
            onChange={handleChange}
            onBlur={handleBlur}
            values={values}
            touched={touched}
            errors={errors}
            label="Bairro"
          />
          <Field
            name="city"
            onChange={handleChange}
            onBlur={handleBlur}
            values={values}
            touched={touched}
            errors={errors}
            label="Cidade"
          />
          <Field
            name="state"
            onChange={handleChange}
            onBlur={handleBlur}
            touched={touched}
            values={values}
            errors={errors}
            label="Estado"
          />

          <Typography variant="h5" style={{ marginTop: "32px" }}>
            <PaymentIcon /> Dados do pagamento
          </Typography>
          <Field
            name="card_holder_name"
            onChange={handleChange}
            onBlur={handleBlur}
            touched={touched}
            values={values}
            errors={errors}
            label="Nome no cartão"
          />
          <Field
            name="card_number"
            onChange={handleChange}
            onBlur={handleBlur}
            values={values}
            touched={touched}
            errors={errors}
            label="Número do cartão"
          />
          <Field
            name="card_cvv"
            onChange={handleChange}
            onBlur={handleBlur}
            values={values}
            touched={touched}
            errors={errors}
            label="CVV (código de segurança)"
          />
          <Field
            type="date"
            name="card_expiration_date"
            onChange={handleChange}
            onBlur={handleBlur}
            values={values}
            touched={touched}
            errors={errors}
            label="Data de expiração"
          />
          <FooterBar>
            <ConfirmPaymentButton
              loading={isSubmitting}
              label="Confirmar compra!"
            />
          </FooterBar>
        </form>
      )}
    </Formik>
  );
};

const CheckoutView = ({ products, onSubmit, completed }) => {
  return (
    <>
      <ConfirmationModal show={completed} products={products} />
      <div style={{ padding: "82px 0 82px 0" }}>
        <AppBar>
          <BackButton to={paths.cart}></BackButton>
        </AppBar>
        <Grid container direction="column" justify="center" alignItems="center">
          <Grid item xs={12} md={6}>
            <CheckoutForm onSubmit={onSubmit} />
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default CheckoutView;
