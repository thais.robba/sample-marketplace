import React, { useState } from "react";
import { Typography, Button, Grid, useMediaQuery } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { view } from "react-easy-state";
import PaymentIcon from "@material-ui/icons/Payment";
import { Link } from "react-router-dom";

import AppBar from "../../components/AppBar";
import BackButton from "../../components/BackButton";
import FooterBar from "../../components/FooterBar";
import ProductsTable from "../../components/ProductsTable";
import { paths } from "../../routes";
import { locale, test } from "./constants";
import theme from "../../theme";
import { formatAsCurrency } from "../../utils";
import globalStore from "../../store";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: xs => (xs ? theme.spacing(7, 0) : theme.spacing(10, 2))
  }
}));

const NoResults = () => {
  return (
    <div
      style={{
        padding: "48px",
        marginTop: "48px",
        color: "darkgrey",
        textAlign: "center",
        whiteSpace: "pre-line"
      }}
    >
      <Typography variant="h4" data-testid={test.ids.noResults}>
        {locale.pt.noResults}
      </Typography>
    </div>
  );
};

const CartView = view(({ products }) => {
  const matches = useMediaQuery(theme.breakpoints.only("xs"));
  const classes = useStyles(matches);

  const hasProducts = products && products.length > 0;

  return (
    <div className={classes.root}>
      <AppBar>
        <BackButton to={paths.home}></BackButton>
      </AppBar>
      {hasProducts ? (
        <>
          <ProductsTable products={products}></ProductsTable>
          <FooterBar>
            <Grid container alignItems="center" justify="center">
              <Grid item xs={4} md={6}>
                <Typography>
                  Total: {formatAsCurrency(globalStore.totalCartCost)}
                </Typography>
              </Grid>
              <Grid item xs={8} md={6} align="right">
                <Button
                  data-testid={test.ids.checkoutButton}
                  endIcon={<PaymentIcon />}
                  component={Link}
                  to={paths.checkout}
                >
                  {locale.pt.goToCheckout}
                </Button>
              </Grid>
            </Grid>
          </FooterBar>
        </>
      ) : (
        <NoResults></NoResults>
      )}
    </div>
  );
});

export default CartView;
