import React from "react";

import CartView from "./CartView";

import globalStore from "../../store";

import { view } from "react-easy-state";

const Cart = view(() => {
  return <CartView products={globalStore.cartItems}></CartView>;
});

export default Cart;
