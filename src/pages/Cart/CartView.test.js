import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router";

import CartView from "./CartView";

import { test } from "./constants";
import { test as globals } from "../../constants";

describe("CartView", () => {
  it("should properly show what is in the store", () => {
    const { asFragment, getAllByTestId } = render(
      <MemoryRouter>
        <CartView
          products={Object.keys(globals.products).map(
            key => globals.products[key]
          )}
        ></CartView>
      </MemoryRouter>
    );

    Object.keys(test.ids)
      .filter(key => key !== "noResults")
      .forEach(key => getAllByTestId(test.ids[key]));

    expect(asFragment()).toMatchSnapshot();
  });

  it("should degrade gracefully if empty", () => {
    const { asFragment, getByTestId } = render(
      <MemoryRouter>
        <CartView products={[]}></CartView>
      </MemoryRouter>
    );

    getByTestId(test.ids.noResults);

    expect(asFragment()).toMatchSnapshot();
  });
});
