import React from "react";
import PropTypes from "prop-types";
import { Button, ButtonGroup, Grid } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { view } from "react-easy-state";

import globalStore from "../../store";

const ProductQuantityButtons = view(({ quantity, id }) => {
  return (
    <Grid item xs={12}>
      <ButtonGroup fullWidth aria-label="full width outlined button group">
        <Button
          onClick={e => {
            if (e) e.preventDefault();
            globalStore.changeCartItemQuantity(id, -1);
          }}
        >
          {quantity > 1 ? "-" : <DeleteIcon fontSize="small" />}
        </Button>
        <Button onClick={() => {}}>
          <p>{quantity}</p>
        </Button>
        <Button
          onClick={e => {
            if (e) e.preventDefault();
            globalStore.changeCartItemQuantity(id, 1);
          }}
        >
          +
        </Button>
      </ButtonGroup>
    </Grid>
  );
});

ProductQuantityButtons.propTypes = {
  quantity: PropTypes.number,
  id: PropTypes.string.isRequired
};

export default ProductQuantityButtons;
