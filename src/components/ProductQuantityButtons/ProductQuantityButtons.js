import React from "react";
import PropTypes from "prop-types";

import ProductQuantityButtonsView from "./ProductQuantityButtonsView";

const ProductQuantityButtons = ({ quantity, id }) => {
  return (
    <ProductQuantityButtonsView
      quantity={quantity}
      id={id}
    ></ProductQuantityButtonsView>
  );
};

ProductQuantityButtons.propTypes = {
  quantity: PropTypes.number.isRequired,
  id: PropTypes.string.isRequired
};

export default ProductQuantityButtons;
