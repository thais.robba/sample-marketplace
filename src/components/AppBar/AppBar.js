import React from "react";
import PropTypes from "prop-types";

import { AppBar as MaterialBar, Toolbar, Typography } from "@material-ui/core";
import { makeStyles, ThemeProvider } from "@material-ui/core/styles";
import CheckBoxOutlineBlankOutlinedIcon from "@material-ui/icons/CheckBoxOutlineBlankOutlined";
import { Link } from "react-router-dom";

import theme from "../../theme";
import { paths } from "../../routes";

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1
  },
  title: {
    flexGrow: 1,
    marginLeft: "2px",
    fontWeight: 700
  }
}));

const AppBar = ({ children }) => {
  const { root, title } = useStyles();

  return (
    <div className={root}>
      <ThemeProvider theme={theme}>
        <MaterialBar>
          <Toolbar>
            <CheckBoxOutlineBlankOutlinedIcon />
            <Typography variant="h5" className={title}>
              <Link
                to={paths.home}
                style={{ textDecoration: "none", color: "black" }}
              >
                marketPlay
              </Link>
            </Typography>

            {children}
          </Toolbar>
        </MaterialBar>
      </ThemeProvider>
    </div>
  );
};

AppBar.propTypes = {
  children: PropTypes.any
};

export default AppBar;
