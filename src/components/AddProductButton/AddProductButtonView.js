import React from "react";
import PropTypes from "prop-types";
import { Button, ButtonGroup } from "@material-ui/core";
import AddShoppingCartOutlined from "@material-ui/icons/AddShoppingCartOutlined";
import { view } from "react-easy-state";

import ProductQuantityButtons from "../ProductQuantityButtons";
import globalStore from "../../store";
import { locale, test } from "./constants";

const AddProductButtonView = view(({ product, variant, color }) => {
  const quantity =
    (globalStore.cart[product.id] && globalStore.cart[product.id].quantity) ||
    0;

  return quantity ? (
    <ProductQuantityButtons id={product.id} quantity={quantity} />
  ) : (
    <ButtonGroup fullWidth>
      <Button
        color={color}
        variant={variant}
        data-testid={test.ids.addToCart}
        endIcon={<AddShoppingCartOutlined />}
        onClick={e => {
          if (e) e.preventDefault();
          globalStore.addToCart(product);
        }}
      >
        {locale.pt.addToCart}
      </Button>
    </ButtonGroup>
  );
});

AddProductButtonView.propTypes = {
  product: PropTypes.object.isRequired,
  variant: PropTypes.string,
  color: PropTypes.string
};

export default AddProductButtonView;
