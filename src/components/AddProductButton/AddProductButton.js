import React from "react";
import PropTypes from "prop-types";

import AddProductButtonView from "./AddProductButtonView";

const AddProductButton = ({ product, variant, color }) => {
  return (
    <AddProductButtonView
      product={product}
      variant={variant}
      color={color}
    ></AddProductButtonView>
  );
};

AddProductButton.propTypes = {
  product: PropTypes.object.isRequired,
  variant: PropTypes.string,
  color: PropTypes.string
};

export default AddProductButton;
