import React from "react";
import { Button } from "@material-ui/core";
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
import Badge from "@material-ui/core/Badge";
import { Link } from "react-router-dom";
import { view } from "react-easy-state";

import globalStore from "../../store";
import { paths } from "../../routes";
import { test, locale } from "./constants";

const ViewCartButton = view(() => {
  const count = globalStore.cartSize;

  return (
    <Button
      data-testid={test.ids.shoppingCart}
      component={Link}
      to={paths.cart}
      endIcon={
        count < 1 ? (
          <ShoppingCartOutlinedIcon />
        ) : (
          <Badge badgeContent={count} color="secondary">
            <ShoppingCartOutlinedIcon />
          </Badge>
        )
      }
    >
      {locale.pt.cart}
    </Button>
  );
});

export default ViewCartButton;
