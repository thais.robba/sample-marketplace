import React from "react";
import PropTypes from "prop-types";

import { AppBar, Toolbar } from "@material-ui/core";
import { makeStyles, ThemeProvider } from "@material-ui/core/styles";

import theme from "../../theme";

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1
  },
  appBar: {
    top: "auto",
    bottom: 0
  }
}));

const MyAppBar = ({ children }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <ThemeProvider theme={theme}>
        <AppBar className={classes.appBar}>
          <Toolbar>{children}</Toolbar>
        </AppBar>
      </ThemeProvider>
    </div>
  );
};

MyAppBar.propTypes = {
  children: PropTypes.any
};

export default MyAppBar;
