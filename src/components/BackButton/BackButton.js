import React from "react";
import PropTypes from "prop-types";
import { Button } from "@material-ui/core";
import ArrowBack from "@material-ui/icons/ArrowBack";
import { Link } from "react-router-dom";

import { test, locale } from "./constants";

const BackButton = ({ to }) => {
  return (
    <Button
      data-testid={test.ids.backButton}
      component={Link}
      to={to}
      startIcon={<ArrowBack />}
    >
      {locale.pt.goBack}
    </Button>
  );
};

BackButton.propTypes = {
  to: PropTypes.string.isRequired
};

export default BackButton;
