import React from "react";
import { render, fireEvent } from "@testing-library/react";

import ProductsTableView from "./ProductsTableView";

import { test } from "./constants";
import { test as globalTest } from "../../constants";
import globalStore from "../../store";

describe("ProductsTableView", () => {
  it("should render a list of products", () => {
    const { asFragment, getAllByTestId } = render(
      <ProductsTableView products={globalTest.products}></ProductsTableView>
    );

    getAllByTestId(test.ids.item);

    // expect(asFragment()).toMatchSnapshot();
  });

  // it("should degrade gracefully", () => {
  //   const { getByTestId } = render(
  //     <ProductCard {...test.product}></ProductCard>
  //   );

  //   const addToCart = getByTestId(test.ids.addToCart);

  //   expect(globalStore.cart.length).toBe(0);
  //   fireEvent.click(addToCart);
  //   expect(globalStore.cart.length).toBe(1);
  // });
});
