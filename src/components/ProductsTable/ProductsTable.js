import React from "react";
import PropTypes from "prop-types";

import ProductsTableView from "./ProductsTableView";

import { view } from "react-easy-state";
const ProductsTable = view(({ products }) => {
  return <ProductsTableView products={products}></ProductsTableView>;
});

ProductsTable.propTypes = {
  products: PropTypes.array.isRequired
};

export default ProductsTable;
