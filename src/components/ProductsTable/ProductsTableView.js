import React from "react";
import PropTypes from "prop-types";
import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
  useMediaQuery
} from "@material-ui/core";
import { view } from "react-easy-state";

import theme from "../../theme";
import { formatAsCurrency } from "../../utils";
import { test } from "./constants";

import ProductQuantityButtons from "../ProductQuantityButtons/ProductQuantityButtons";

const TotalPrice = ({ price, quantity = 0 }) => (
  <Grid item xs={12}>
    {quantity <= 1 ? (
      <Typography variant="h6" align="right">
        {formatAsCurrency(price)}
      </Typography>
    ) : (
      <>
        <Typography align="right">
          {quantity} x {formatAsCurrency(price)}
        </Typography>
        <Typography variant="h6" align="right">
          {formatAsCurrency(quantity * price)}
        </Typography>
      </>
    )}
  </Grid>
);

const ItemNameAndOrigin = ({ name, recipient_id }) => (
  <TableCell align="left">
    <Grid container>
      <Grid item xs={12}>
        <Typography variant="h6" align="left">
          {name}
        </Typography>
        <Typography align="left">Vendido por: {recipient_id}</Typography>
      </Grid>
    </Grid>
  </TableCell>
);

const ProductImage = ({ src }) => {
  const matches = useMediaQuery(theme.breakpoints.only("xs"));

  return (
    <TableCell>
      <img src={src} width={matches ? 42 : 96} />
    </TableCell>
  );
};

const ProductRows = ({ id, src, name, recipient_id, price, quantity = 0 }) => (
  <TableRow key={name} data-testid={test.ids.item}>
    <ProductImage src={src} />
    <ItemNameAndOrigin name={name} recipient_id={recipient_id} />
    <TableCell align="right">
      <Grid container alignItems="flex-end">
        <TotalPrice price={price} quantity={quantity} />
        <ProductQuantityButtons id={id} quantity={quantity} />
      </Grid>
    </TableCell>
  </TableRow>
);

const ProductsTableView = view(({ products }) => {
  return (
    <Paper>
      <Table>
        <TableBody>
          {products.map((product, i) => (
            <ProductRows {...product} key={i} />
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
});

ProductsTableView.propTypes = {
  products: PropTypes.array.isRequired
};

export default ProductsTableView;
