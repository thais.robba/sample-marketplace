import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router";

import ProductCard from "./ProductCard";
import { test } from "./constants";
import { test as globals } from "../../constants";

import globalStore from "../../store";

describe("ProductCard", () => {
  beforeEach(() => {
    globalStore.emptyCart();
  });

  afterAll(() => {
    globalStore.emptyCart();
  });

  it("should render the basic product information", () => {
    const { asFragment, getByTestId } = render(
      <MemoryRouter>
        <ProductCard product={globals.products[0]}></ProductCard>
      </MemoryRouter>
    );

    Object.keys(test.ids).forEach(key => {
      getByTestId(test.ids[key]);
    });

    expect(asFragment()).toMatchSnapshot();
  });
});

export default ProductCard;
