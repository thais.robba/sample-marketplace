import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardActionArea,
  useMediaQuery,
  CardHeader,
  CardContent,
  CardMedia,
  Button,
  Typography,
  Grid
} from "@material-ui/core";
import { makeStyles, ThemeProvider, useTheme } from "@material-ui/core/styles";
import { view } from "react-easy-state";
import { Link } from "react-router-dom";

import { paths } from "../../routes";
import { test, locale } from "./constants";
import theme from "../../theme";
import { formatAsCurrency } from "../../utils";
import globalStore from "../../store";
import ProductQuantityButtons from "../ProductQuantityButtons/ProductQuantityButtonsView";
import AddProductButton from "../AddProductButton";

const useStyles = makeStyles({
  card: {
    maxWidth: 345
  },
  media: {
    height: 140,
    minWidth: 280
  },
  details: {
    marginTop: "6px"
  }
});

const Price = ({ price, value }) => {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.only("xs"));

  return (
    <Grid container justify="space-between">
      {value !== price ? (
        <Grid item>
          <s>
            <Typography
              data-testid={test.ids.value}
              variant={matches ? null : "h6"}
              color="secondary"
              component="h4"
              align="right"
              style={{ fontFamily: "Calistoga" }}
            >
              De {formatAsCurrency(value)}
            </Typography>
          </s>
        </Grid>
      ) : null}
      <Grid item>
        <Typography
          data-testid={test.ids.price}
          gutterBottom
          variant={matches ? "h6" : "h4"}
          component="h4"
          align="right"
          style={{ fontFamily: "Calistoga" }}
        >
          {formatAsCurrency(price)}
        </Typography>
      </Grid>
    </Grid>
  );
};

const ProductCard = ({ product }) => {
  const classes = useStyles();
  const { src, name, price, value, id } = product;
  const quantity = globalStore.cart[id] && globalStore.cart[id].quantity;

  return (
    <ThemeProvider theme={theme}>
      <li data-testid={test.ids.item}>
        <Card className={classes.card}>
          <CardHeader
            data-testid={test.ids.name}
            action={
              <Button
                className={classes.details}
                component={Link}
                to={paths.productDetails.replace(":id", id)}
                data-testid={test.ids.viewDetails}
              >
                {locale.pt.viewDetails}
              </Button>
            }
            title={name}
            titleTypographyProps={{
              style: { fontFamily: "Calistoga" }
            }}
          />
          <CardActionArea onClick={() => {}}>
            <CardMedia
              className={classes.media}
              data-testid={test.ids.img}
              image={src}
              title={`Picture of the product ${name}`}
            />
          </CardActionArea>
          <CardContent>
            <Price price={price} value={value}></Price>
            <AddProductButton
              product={product}
              variant="contained"
              color="primary"
            />
          </CardContent>
        </Card>
      </li>
    </ThemeProvider>
  );
};

ProductCard.propTypes = {
  product: PropTypes.object.isRequired
};

export default view(ProductCard);
