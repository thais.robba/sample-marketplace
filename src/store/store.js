import { store } from "react-easy-state";

const globalStore = store({
  cart: {},
  get cartItems() {
    return Object.keys(globalStore.cart).map(key => globalStore.cart[key]);
  },
  get cartSize() {
    return Object.keys(globalStore.cart).length;
  },
  get totalCartCost() {
    const keys = Object.keys(globalStore.cart);

    if (keys.length <= 0) return 0;

    return keys
      .map(key => {
        const { price, quantity } = globalStore.cart[key];

        return quantity * price;
      })
      .reduce((sum, value) => sum + value);
  },
  addToCart: (product, quantity = 1) => {
    const { id } = product;
    const { cart } = globalStore;

    if (!cart[id]) {
      cart[id] = { ...product, quantity };
    } else {
      cart[id].quantity += quantity;
    }
  },
  changeCartItemQuantity: (product, amount) => {
    const id = typeof product === "object" ? product.id : product;
    const { cart } = globalStore;

    if (cart[id]) {
      cart[id].quantity += amount;

      if (cart[id].quantity <= 0) globalStore.removeFromCartById(id);
    } else {
      //We probably should handle it when there is something amiss
    }
  },
  removeFromCart: ({ id }) => {
    globalStore.removeFromCartById(id);
  },
  removeFromCartById: id => {
    delete globalStore.cart[id];
  },
  emptyCart: () => (globalStore.cart = {})
});

export default globalStore;
