import { test } from "../constants";
import globalStore from "./store";

const mockProduct = test.products[0];

const addMockItemToCart = (expectQuantity = 1) => {
  globalStore.addToCart(mockProduct);
  expectCartToHaveProductQuantity(expectQuantity);
};

const expectCartToHaveProductQuantity = expectQuantity => {
  expect(globalStore.cart).toEqual({
    [mockProduct.id]: { ...mockProduct, quantity: expectQuantity }
  });
};

const expectCartToBeEmpty = () => {
  expect(globalStore.cart).toEqual({});
};

describe("globalStore", () => {
  beforeEach(() => {
    globalStore.cart = {};
    expectCartToBeEmpty();
  });
  afterAll(() => {
    globalStore.cart = {};
  });

  it("should start with an empty cart", () => {
    expectCartToBeEmpty();
  });

  it("should allow us to add an item to the cart", () => {
    addMockItemToCart();
  });

  it("should allow us to add an item to the cart multiple times", () => {
    addMockItemToCart(1);
    addMockItemToCart(2);
    addMockItemToCart(3);
  });

  it("should allow us to remove an item from the cart", () => {
    addMockItemToCart();
    globalStore.removeFromCart(mockProduct);
    expectCartToBeEmpty();

    addMockItemToCart();
    globalStore.removeFromCartById(mockProduct.id);
    expectCartToBeEmpty();
  });

  it("should allow us to empty the cart", () => {
    addMockItemToCart();
    globalStore.emptyCart();
    expectCartToBeEmpty();
  });

  it("should allow us to change an items quantity", () => {
    addMockItemToCart();
    globalStore.changeCartItemQuantity(mockProduct, -1);
    expectCartToBeEmpty();

    addMockItemToCart();
    globalStore.changeCartItemQuantity(mockProduct, 9);
    expectCartToHaveProductQuantity(10);
  });

  it("should calculate price total", () => {
    addMockItemToCart();
    globalStore.changeCartItemQuantity(mockProduct, 9);

    expect(globalStore.totalCartCost).toBe(10 * mockProduct.price);
  });
});
