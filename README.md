# MarketPlay

Welcome to the most playful market this side of the 'verse so... _stick_ around!

## Getting started

Add a test API key to the `.env.dev` file

Make sure to have yarn `1.12` or later installed.

On your terminal, execute the following command:

        yarn install && yarn start

After the machine is done with this incantation, it will open the portal for playful shopping. Alternatively, you can access `localhost:1234` in your browser of choice.

## Testing

Integrating with pipelines or modifying features? Be sure to run:

        yarn test

It works with all standard CLI parameters for Jest, so snapshot updating, tags and whatnot are all available for use.

## Deploying

When you are confident that it is time to make a release, run:

        yarn build

This will create a small artifact, aptly in the `dist` folder that can be uploaded and used in a server. Huzzah!

## Tech choices

Besides the pre-requisites, I opted for some less common or new (to me) technologies and I thought it would be interesting to point out why.

### Parcel

Webpack is powerful, it is the standard, sure. But it is also a complete pain to upgrade between versions and the plethora of options it has means that even veterans will have a hard time configuring and modifying it - specially with time gaps of months. Parcel, on the other hand, is fast, often zero config, easier to use and uses as many cpu cores as possible for a snappier build process.

For webpack I would have likely gone with something that is far more opaque and harder to manager (like Create React App). With Parcel I just implemented the build system from scratch in a few minutes.

I think people vastly undervalue the advantages of a transparent system and separation of concerns but I digress.

### react-easy-state

Redux is a powerful tool but it's concept clearly wasn't well translated to JavaScript. Verbose, without the safety and ease of use as of the languages it copies - it is akin to the concept of 'Action at a distance'. Hooks definitely make it less painful to use but why settle for 'less painful' if we can have 'joyful'?

Enter react-easy-state, a proxy-based data store utility that allows us to use plain looking semantics all the while batching changes for React.

"Blasphemous! Imperative!"

Actually it does a lot of things internally the same way as Redux, meaning that it only looks like you are mutating an object imperatively but you are actually queueing changes. That is the beauty of proxies - they allow us to deliver a much better API while keeping performance and constraining mutability. Downside is that Internet Explorer has no support for it, limiting places where the technique can be applied.

Check `src/store/store.test.js` to see what I mean by it being easy to test :)

### index.js with export default

This is mostly a readability/editor convenience feature. It allows us to do simpler imports.

        import Page from '../Page'

Is certainly a better API than:

        import Page from '../Page/Page'

Plus, it can be easily automated.

### Page -> View separation

This one will certainly look odd to many. The basic idea is that, to have easily testable views, all data handling lives at the top level, while all rendering happens on the secondary layer.

This means that our view components can be 'dumb' and highly flexible when needed. This decoupling makes refactoring a bit easier as we don't have to hunt data handling all over the place but we can keep our components, well, 'componetized'.

In my haste to approach a more "finished" prototype, not all pages/components have perfect separation - nor tests, for that matter :(

### JSON constants

Not everyone likes this but I see it's major advantage being that this stops us from being overly clever when simplicity suffices. Having shared data between tests and production components is essential to avoid build-breaking "oops forgot to update the string there" mistakes.

It also means that, should we want to have A/B testing, we are already thinking of simple data structures that could be returned from a server. Of course, we will probably never want to change the `test` portion of the object but localization, number of entries and things like that.

### Material-UI

I thought of using Styled Components (quite like it!) but decided against it, as creating every style from scratch would be quite time consuming and I took this as a chance to try a different framework, the aforementioned Material UI.

I kinda regret that.

Material UI is quite featureful and well documented but it is an extremely verbose library with some odd cases. It feels like a continuation of React's `className` and `styleObject` philosophy of styling, which I feel like it is the wrong direction to go. If anything, we are being taught over and over again that either we stick to CSS (like Svelte or Styled Components) or we create something new that actually eases developement (like elm-ui or Reason's Revery).

### Formik

I used it mostly out of convenience and to check what all the buzz is about - by all means, it is a really nice library but I can't help but feel that it is... lacking. It clearly loses to a more structured, view library minded approach. For example:

```javascript
<Form
  onSubmit={({ username, password }) => postLogin(username, encrypt(password))}
>
  <UsernameField required />
  <PasswordField required />
</Form>
```

You can probably guess what is happening about, just from looking at it. What if I told you that this also applies standard validation to the fields, renders the standard submission button and disables submission if any rules are broken (ie: required field is empty)?

This can be achieved by carefully exposing a React.Context to a field data-binding hook and frankly, ends up being a much nicer API. Downside is the initial cost is relatively high and requires a lot of extra tests but maintenance itself is not too hard nor too costly.

## Niceties, Refactors & Essentials

- I would probably add a project-wide command to create new pages/components
- Refactoring the global store to be passed would allow for more independent components - webcomponents and the like wouldn't have straightforward acess to `react-easy-state` and it would be an important step towards micro frontend.
- Constants are great! But what is the ideal format when a test-id is desired in more than one place? Here my `json` file technique falls short, can't really import values from other `json` files.
- The project is missing deep integration tests! I would probably add Cypress, creating tests for possible API failure points. For example: suppose a `GET` request, we would have one test simulating its failure and one for the 'happy path'.
- Product cards should change the button that is displayed when they are added to cart, probably something that quickly allowed the user to increment/decrement the amount of the item that they want to get (this might not make sense in certain business domains of high value, low volume items).
- UI feedback is currently quite poor. Ideally things would react expressively (example: cart badge should animate when adding more items to cart)
- Checkout page was quite rushed, no doubt about it. It is missing validation for (almost) all fields, along with better styling and masking of the fields.
- Tests. I'd love to have a lot more tests - ideally, with more time, I'd add Cypress tests and vastly improve the Jest & react-testing-library coverage.

## Assets

Images used are from [Kenney's image packs](https://www.kenney.nl/assets/animal-pac), available as CC0.
